# encoding: UTF-8
# frozen_string_literal: true

require 'net/http'
require 'uri'

# User variables
##############

MEASUREMENT_INTERVAL = 15
INFLUXDB_SERVER = "http://127.0.0.1:8086"

W1_SYS_FLDR = "/sys/bus/w1/devices"

DATABASE = "weatherlog"
MEASUREMENT = "climate"

NAME_MAPPINGS = Hash.new {|hash, key| key}
# Define mappings here for different sensor names

# Simple logging script for DS18B20 temperature sensors
##############

module DS18B20Logger
  def self.interval()
    loop do
      yield
      sleep MEASUREMENT_INTERVAL
    end
  end

  def self.ingest_and_send(serial_path)
    begin
      read_temperature = File.read("#{W1_SYS_FLDR}/#{serial_path}/temperature").strip.to_f / 1000.0
      
      # Bodge together an InfluxDB line protocol statement
      line = "#{MEASUREMENT},location=#{NAME_MAPPINGS[serial_path]} temperature=#{read_temperature}"

      response = Net::HTTP.post URI("#{INFLUXDB_SERVER}/write?db=#{DATABASE}"), line
      puts "Received response: #{response.inspect}"
    rescue => e
      STDERR.puts "Failed to ingest with device #{serial_path}: #{e.inspect}"
    end
  end

  def self.capture()
    begin
      Dir.foreach(W1_SYS_FLDR) do |fldr|
        # Only accept entries for DS18B20
        if /\A28-(?<serial>[[:xdigit:]]{12})\Z/i =~ fldr
          #puts "Found device, reading from: #{serial}"
          ingest_and_send "28-#{serial}"
        end
      end
    rescue => e
      STDERR.puts "Could not enumerate: #{e.inspect}. Skipping this cycle"
    end
  end

  def self.run()
    interval do
      puts "Capturing temperature"
      capture
    end
  end
end

Signal.trap("INT") { exit }

DS18B20Logger::run()
