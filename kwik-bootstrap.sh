#!/bin/sh
# shellcheck shell=dash

set -u

# This script bootstraps an Alpine Linux image for Raspberry Pi, doing necessary initialization and preloading necessary data

##################################################################################################################
##  USER MODIFIABLE VARIABLES BEGIN

# What we should download?
BASE_ALPINE_RASPI_ARCH=aarch64
BASE_ALPINE_RASPI_IMAGE="${BASE_ALPINE_RASPI_IMAGE:-https://dl-cdn.alpinelinux.org/alpine/v3.13/releases/$BASE_ALPINE_RASPI_ARCH/alpine-rpi-3.13.1-$BASE_ALPINE_RASPI_ARCH.tar.gz}"

# How large the card size is expected to be in gigabytes? Unlike NOOBS or some other Raspberry Pi distros, the image created here will not expand automatically
# This can also be done after the image is created, but if one knows how large the card is, it can be a convenience to do it here
EXPECTED_CARD_SIZE=2

##  USER MODIFIABLE VARIABLES END
##################################################################################################################

# Internal constants
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")



# Emit a standardized log message
say() {
  printf 'kwik-bootstrap: %s\n' "$1"
}

# Trigger a fatal error - stop immediately after
fatal() {
  say "$1" >&2
  say "cannot continue, exiting" >&2
  exit 1
}

# Check the existence of a command, only capturing the exit status
check_cmd_existence() {
    command -v "$1" > /dev/null 2>&1
}

# Require a command - if not found, trigger a fatal error
require_cmd() {
  if ! check_cmd_existence "$1"; then
    fatal "requires '$1' but could not be found"
  fi
}

sudo_or_root() {
  if [ "$(id -u)" -ne 0 ]; then
    sudo "$@"
  else
    "$@"
  fi
}

# Run a command that should not fail. If it does so, then stop immediately
ensure() {
    if ! "$@"; then 
      fatal "command failed to run: $*" 
    fi
}

# Download with wget
# Arguments: URL, save location
download() {
  local _err
  local _status
  
  if [ "$SKIP_DOWNLOAD_IF_EXISTENT" = "yes" ] && [ -f "$2" ]; then
    return 0
  fi

  # Attempt to retrieve
  _err=$(wget -q --https-only "$1" -O "$2" 2>&1)
  _status=$?

  # Emit error where appropriate
  if [ -n  "$_err" ]; then
    echo "$_err" >&2
  fi

  return $_status
}

# Assert that certain common, absolutely required commands exist
require_common_commands() {
  require_cmd mkdir
  require_cmd rm
  require_cmd rmdir
  require_cmd mktemp
  require_cmd realpath

  require_cmd fallocate
  require_cmd fdisk
  require_cmd losetup
  require_cmd mkfs.fat

  require_cmd tar
  require_cmd wget

  # Check that if we run as non-root, we can access sudo
  if [ "$(id -u)" -ne 0 ]; then
    require_cmd sudo
    say "Running as non-root user. Expect to be required to enter your credentials, as some operations (e.g. loop-mounting) require superuser privileges"
  fi
}

# Compose overlay, adding necessary files for first-boot operations
compose_overlay() {
  
  local _staging_overlay_dir
  _staging_overlay_dir="$1"

  # Copy over all first boot scripts
  ensure cp -R "$SCRIPTPATH/first_boot_scripts/overlay/." "$_staging_overlay_dir"

  # Ensure runnability
  ensure chmod 755 "$_staging_overlay_dir/etc/local.d/"*.start
  ensure chmod 755 "$_staging_overlay_dir/etc/kwik_install_scripts/"*.sh

  if [ -n "${USER_OVERLAY_DIRECTORY:-}" ]; then
    say "User installation configuration directory provided, copying to overlay"
    ensure cp -R "$USER_OVERLAY_DIRECTORY" "$_staging_overlay_dir/etc/kwik_install_scripts/user_overlay"
  fi

  # Create symlink for OpenRC
  ensure mkdir -p "$_staging_overlay_dir/etc/runlevels/default"
  ensure ln -s "/etc/init.d/local" "$_staging_overlay_dir/etc/runlevels/default/local"
}

# Prepare file systems; boot and storage dev 
prepare_fs() {
  local _boot_dev
  _boot_dev="$1"
  local _storagefs_dev
  _storagefs_dev="$2"

  ensure sudo_or_root mkfs.fat -F32 "$_boot_dev"
  # If desired, this can be re-enabled - but as of now, the system will automatically format on first install
  # ensure sudo_or_root mkfs.ext4 "$_storagefs_dev"
}

# Prepare a loopmount image with a base system
# Arguments: loopmount image file, downloaded RPi image, staging directory as an absolute directory
prepare_image() {
  local _loopmount_image_file
  _loopmount_image_file="$1"

  local _raspi_image_file
  _raspi_image_file="$2"

  local _staging_dir
  _staging_dir="$3"
  
  # Clean up loopmount file if existing
  if [ -f "$_loopmount_image_file" ]; then
    ensure rm "$_loopmount_image_file"
  fi

  say "Creating new image at: $_loopmount_image_file"

  ensure fallocate -l "$EXPECTED_CARD_SIZE"G "$_loopmount_image_file"

  # Emit FDISK commands. Create a 512M boot partition (allocate spare space for updates and installation), rest except 1G for data, and last for swap
  ensure cat > "$_staging_dir/fdisk_init.cmd" <<-EOF
    o
    n
    p
    1

    +256M
    t
    0c
    a
    n
    p
    2

    -1G
    n
    p
    3

    
    t
    3
    82
    w
EOF

  ensure fdisk "$_loopmount_image_file" < "$_staging_dir/fdisk_init.cmd"

  # Next, prepare an overlay
  say "Preparing overlay files"

  local _staging_overlay_dir
  _staging_overlay_dir="$_staging_dir/overlay"

  if [ -d "$_staging_overlay_dir" ]; then
    ensure rm -rf "$_staging_overlay_dir"
  fi

  ensure mkdir -p "$_staging_overlay_dir"

  # Compose overlay
  compose_overlay "$_staging_overlay_dir"

  # Package into APKOVL
  say 'Packaging overlay data into a APKOVL file...'
  local _staging_apkovl
  _staging_apkovl="$_staging_dir/kwik_first_boot.apkovl.tar.gz"

  ensure tar -c -z --owner=0 --group=0 -f "$_staging_apkovl" -C "$_staging_overlay_dir" etc

  # Adding optional tools to boot media
  local _staging_opttools
  _staging_opttools="$_staging_dir/kwik_opttools.tar.gz"

  say 'Creating tarball for optional tools'
  ensure tar -c -z --owner=0 --group=0 -f "$_staging_opttools" -C "$SCRIPTPATH/opt_tools" .

  say 'Loop-mounting blank disk image and initializing it...'

  local _loop_dev
  _loop_dev=$(ensure sudo_or_root losetup --partscan --show --find "${_loopmount_image_file}")
  local _boot_dev
  _boot_dev="$_loop_dev"p1
  local _storagefs_dev
  _storagefs_dev="$_loop_dev"p2

  # Prepare FS
  prepare_fs "$_boot_dev" "$_storagefs_dev"

  # Mount boot partition
  local _boot_dev_mount
  _boot_dev_mount="$_staging_dir/boot_mount"
  ensure sudo_or_root mkdir -p "$_boot_dev_mount"
  ensure sudo_or_root mount "$_boot_dev" "$_boot_dev_mount"

  # Extract files to loop mount
  ensure sudo_or_root tar xf "$_raspi_image_file" --no-same-owner -C "$_boot_dev_mount"

  # Copy overlay and opttools over
  ensure sudo_or_root cp "$_staging_apkovl" "$_boot_dev_mount"/
  ensure sudo_or_root cp "$_staging_opttools" "$_boot_dev_mount"/
  # Cleanup
  sudo_or_root umount "$_boot_dev_mount"
  sudo_or_root rmdir "$_boot_dev_mount"
  ensure sudo_or_root losetup -d "$_loop_dev" 
}

parse_opts() {
  while getopts 'a:f:d:s:u:o:e' c
  do
    case $c in
      a) BASE_ALPINE_RASPI_ARCH="$OPTARG" ;;
      f) BASE_ALPINE_RASPI_IMAGE="$OPTARG" ;;
      d) STAGING_DIRECTORY="$OPTARG" ;;
      s) EXPECTED_CARD_SIZE="$OPTARG" ;;
      u) USER_OVERLAY_DIRECTORY="$OPTARG" ;;
      o) OUTPUT_FILE="$OPTARG" ;;
      e) SKIP_DOWNLOAD_IF_EXISTENT="yes" ;;
      *) say "Usage: -a ARCH -f RASPI_DOWNLOAD_IMAGE -d STAGING_DIRECTORY -s EXPECTED_CARD_SIZE_G -u USER_CONFIGURATION"; exit 1 ;;
    esac
  done
}

main() {
  require_common_commands
  parse_opts "$@"

  say "Downloading image for architecture '$BASE_ALPINE_RASPI_ARCH' to a temporary directory"

  # Set up a temporary directory
  local _staging_dir
  if [ -n "${STAGING_DIRECTORY:-}" ]; then
    say "Using user-specified staging directory: $STAGING_DIRECTORY"
    _staging_dir="$STAGING_DIRECTORY"
  else
    _staging_dir="$(mktemp -d 2>/dev/null || ensure mktemp -d -t kwik-bootstrap)"
    say "Creating new temporary staging directory: $_staging_dir"
  fi

  local _raspi_image_file
  _raspi_image_file="${_staging_dir}/raspi_base_image_$BASE_ALPINE_RASPI_ARCH.tar.gz"

  ensure mkdir -p "$_staging_dir"
  ensure download "$BASE_ALPINE_RASPI_IMAGE" "$_raspi_image_file"

  # Convert staging directory to an absolute one, if it is not yet so
  _staging_dir=$(realpath "$_staging_dir")

  local _loopmount_image_file
  _loopmount_image_file="${_staging_dir}/loopmount_image.img"

  say "Download complete, preparing a $EXPECTED_CARD_SIZE gigabyte image"
  ensure prepare_image "$_loopmount_image_file" "$_raspi_image_file" "$_staging_dir"

  local _final_output_file
  _final_output_file="${OUTPUT_FILE:-./image.img}"

  ensure cp "$_loopmount_image_file" "$_final_output_file"

  say "All done, copied final output to $_final_output_file"
}

main "$@" || exit 1