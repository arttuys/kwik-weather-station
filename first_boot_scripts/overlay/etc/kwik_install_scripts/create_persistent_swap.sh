#!/bin/sh

if free | awk '/^Swap:/ {exit !$2}'; then
  echo "Swap space already exists, refusing to do anything"
  # Swap space exists, don't need to do anything
  exit 0
fi

set -e

mkswap /dev/mmcblk0p3
echo "/dev/mmcblk0p3 none swap sw 0 0" >> /etc/fstab
rc-update add swap boot
rc-service swap start