#!/bin/sh

. /etc/kwik_install_scripts/utils.sh

source_user_overlay_env

set -e

# Install Influxdb; we specficially require a 1.x version for this install
apk add 'influxdb=~1'

# Do some configuration alterations. These essentially alter behavior to be better for embedded systems - defaults assume a full-size PC
# Also do timeouts for queries, preventing runaway events
sed -i "s/.*reporting-disabled.*/reporting-disabled = true/g" /etc/influxdb.conf
sed -i 's/.*index-version.*/index-version = "tsi1"/g' /etc/influxdb.conf
sed -i 's/.*cache-max-memory-size.*/cache-max-memory-size = "75m"/g' /etc/influxdb.conf
sed -i 's/.*query-timeout.*/query-timeout = "60s"/g' /etc/influxdb.conf

# Create data folders, and change ownership
mkdir -p /var/lib/influxdb
chown influxdb:influxdb /var/lib/influxdb

mkdir -p /var/log/influxdb
chown influxdb:influxdb /var/log/influxdb

rc-update add influxdb default

if [ -z "${NO_INITIAL_DB_CREATION:-}" ] ; then
  apk add curl
  rc-service influxdb start

  set +e

  # Wait a few moments for the database to come up. It should not take too long
  sleep 5

  curl -i -XPOST http://localhost:8086/query --data-urlencode "q=CREATE DATABASE weatherlog"

  set -e

  # Stop InfluxDB for rest of installation
  rc-service influxdb stop
fi