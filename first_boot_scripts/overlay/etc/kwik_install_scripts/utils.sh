#!/bin/sh

# Common functions required in multiple locations

source_user_overlay_env() {
  _user_overlay_env_path=${USER_OVERLAY_ENV_SOURCE:-"/etc/kwik_install_scripts/user_overlay/env_params"}
  if [ -f "$_user_overlay_env_path" ] ; then
    . "$_user_overlay_env_path"
  fi
}

#apk_install_and_include_lbu() {
#  apk add "$1" && lbu include $(apk info "$1" -L | tail +2 | sed 's/^/\//')
#}

die() {
  exit 1
}