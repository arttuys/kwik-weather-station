#!/bin/sh

# Source common utils
. /etc/kwik_install_scripts/utils.sh

make_overlay() {
  mkdir -p /media/persistent/overlays/"$1" /media/persistent/overlays/"$1"_workdir
  echo "overlay /$1 overlay lowerdir=/$1,upperdir=/media/persistent/overlays/$1,workdir=/media/persistent/overlays/$1_workdir 0 0" >> /etc/fstab || die
}
# Set up mounts, and make boot media writable 
set_up_mounts_preinstall() {
  mount /media/mmcblk0p1 -o rw,remount || die
  echo '/dev/mmcblk0p1 /media/mmcblk0p1 vfat rw 0 0' >> /etc/fstab || die

  # Prepare for APK caching
  mkdir -p /media/mmcblk0p1/apks_s2
}

set_up_mounts_postinstall() {
  mkdir -p /media/persistent || die

  apk add e2fsprogs || die
  mkfs.ext4 /dev/mmcblk0p2 || die

  mount /dev/mmcblk0p2 /media/persistent -o rw || die
  echo '/dev/mmcblk0p2 /media/persistent ext4 rw 0 0' >> /etc/fstab || die

  # In addition, mount various useful directories as overlays
  make_overlay home
  make_overlay opt
  make_overlay srv
  make_overlay var
  make_overlay root
  mount -a

  # Touch and mark as boot repository
  touch /media/mmcblk0p1/apks_s2/.boot_repository
}

source_user_overlay_env

# We may restart daemons during this process. Do not allow it, only trigger initial setup process ONCE
mv "/etc/local.d/kwik_firstboot.start" "/etc/local.d/kwik_firstboot.retry"

# Set up clock daemons
rc-update add swclock boot    # enable the software clock
rc-update del hwclock boot    # disable the hardware clock

# Initially, set up mounts to allow writing to boot partition
set_up_mounts_preinstall

if [ -f "/etc/kwik_install_scripts/user_overlay/answers" ]; then
  # If defined, concatenate these into defaults
  cat "/etc/kwik_install_scripts/user_overlay/answers" >> /etc/kwik_install_scripts/install_default_answers
fi

# Run Alpine setup, and implicitly install with default answers
setup-alpine -e -f /etc/kwik_install_scripts/install_default_answers || die

# Once we have network connectivity etc, do rest
set_up_mounts_postinstall

# Set up root credentials and SSH keys
(
  echo "Setting up sudo"
  apk add sudo || die
  adduser -h /home/forecaster -s /bin/ash -D forecaster || die
  echo "forecaster:${SET_INITIAL_PASSWORD:-itthunderstoday}" | chpasswd

  echo '%wheel ALL=(ALL) ALL' > /etc/sudoers 
  adduser forecaster wheel
  
  # Finally, remove password from root user
  passwd -l root

  # Prevent SSH root logins
  sed -i "s/.*PermitRootLogin.*/PermitRootLogin no/g" /etc/ssh/sshd_config
) || die

if [ -n "$USER_SSH_KEY" ]; then
  mkdir -p /home/forecaster/.ssh

  chmod 0700 /home/forecaster/.ssh
  chown forecaster:forecaster /home/forecaster/.ssh 
  echo "$USER_SSH_KEY" > /home/forecaster/.ssh/authorized_keys
  chmod 0600 /home/forecaster/.ssh/authorized_keys
  chown forecaster:forecaster /home/forecaster/.ssh/authorized_keys
fi

# Commit initial successful install
echo "Committing initial install configuration..."

echo "Initial setup completed, but something went wrong with automated tool installs (or it was skipped). Sign in with default credentials to complete installation manually or clear this message (from /etc/issue)" > /etc/issue
echo " " >> /etc/issue

lbu_commit -d || die

echo "Starting tool auto-installs"
if [ -z "${NO_AUTOMATIC_INSTALL:-}" ] && sh "/etc/kwik_install_scripts/run_automatic_installs.sh"; then
  # Replace /etc/issue
  echo "Welcome to Kwik-Weather-Station / $(sed -n '/^Serial/{s/.* //;p}' /proc/cpuinfo)." > /etc/issue
  echo " " >> /etc/issue

  # Commit again if automatic installs succeeded
  lbu_commit -d || die
  echo "Automatic installation succeeded, committed"
else
  echo "Automatic installation did not succeed or was skipped, staying with initial configuration upon reboot"
fi

# Copy logs to persistent media
mkdir -p /media/persistent/firstboot_logs
cp "/var/log/firstboot"* /media/persistent/firstboot_logs/

reboot now