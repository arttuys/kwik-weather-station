#!/bin/sh
# shellcheck shell=sh

. /etc/kwik_install_scripts/utils.sh

source_user_overlay_env

if [ -z "${NO_CREATE_SWAP}" ] ; then
    "/etc/kwik_install_scripts/create_persistent_swap.sh" || die
fi

if [ -z "${NO_AUTO_UPGRADE:-}" ] ; then
    set -e

    sed -i '/v3.13/s/^#//' /etc/apk/repositories

    apk update
    apk upgrade
fi

if [ -z "${NO_ONEWIRE_TEMPERATURE:-}" ] ; then
  (
    set -e

    echo "dtoverlay=w1-gpio" >> /media/mmcblk0p1/usercfg.txt

    cat > "/etc/local.d/load_w1_therm.start" <<-EOF
    modprobe w1_gpio
    modprobe w1_therm
EOF
  )
fi

if [ -z "${NO_BLUETOOH}" ] ; then
  apk add bluez bluez-btmon

  sed -i '/-B \/dev\/$MDEV -P bcm/s/^#//' /etc/mdev.conf
fi

if [ -z "${NO_OPTTOOLS}" ] ; then
  apk add ruby ruby-bundler

  mkdir -p /opt/kwik

  tar xfz "/media/mmcblk0p1/kwik_opttools.tar.gz" -C /opt/kwik

  chmod +x "/opt/kwik/"**/*.rb
fi

if [ -z "${NO_INSTALL_INFLUXDB:-}" ] ; then
  "/etc/kwik_install_scripts/install_influxdb.sh" || die
fi

if [ -z "${NO_INSTALL_GRAFANA:-}" ] ; then
  "/etc/kwik_install_scripts/install_grafana.sh" || die
fi

true